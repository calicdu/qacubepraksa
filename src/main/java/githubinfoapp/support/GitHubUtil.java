package githubinfoapp.support;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import githubinfoapp.model.GitHubConnection;
import githubinfoapp.web.dto.PullRequestDto;

public abstract class GitHubUtil {
	
	
	private static String buildRootUrl (String rootUrl) {
		
		String convertedUrl = "";
		
		if (rootUrl.toLowerCase().contains("github.com")) {				
			convertedUrl = "https://api.github.com";			
		}else {
						String[] urlPartials = rootUrl.split("://");
			urlPartials[1] = urlPartials[1].concat("/api/v3/");
			convertedUrl = urlPartials[0].concat("://").concat(urlPartials[1]);		
		}	
		return convertedUrl;		
	}
	
	
	private static String buildPullsUrl (String rootUrl, String owner, String project, String branchName) {
		
		StringBuilder sb = new StringBuilder(buildRootUrl(rootUrl));
		
		sb.append("/repos/");
		sb.append(owner);
		sb.append("/");
		sb.append(project);
		sb.append("/pulls?base=");
		sb.append(branchName);
		sb.append("&state=open&sort=created&direction=desc&per_page=10");						
		String url = sb.toString().replaceAll("(?<!(http:|https:))/+", "/");
		
		return url;
		
	}
	
	private static String buildInitialsUrl (String rootUrl, String username) {
		
		StringBuilder sb = new StringBuilder(buildRootUrl(rootUrl));		
		sb.append("/users/");
		sb.append(username);
		String url = sb.toString().replaceAll("(?<!(http:|https:))/+", "/");
	
		return url;	
	}
	
	private static String buildToken (String rawToken) {		
		String token = "token " + rawToken;	
		return token;		
	}
	
	private static StringBuilder getRawData (String url, String token) throws Exception {
		
		URL endPoint = new URL(url);
        URLConnection urlConnection = endPoint.openConnection();     
        urlConnection.setRequestProperty("Authorization", token);
        urlConnection.setRequestProperty("User-Agent", "");
        
        BufferedReader in = new BufferedReader (new InputStreamReader(urlConnection.getInputStream(),"UTF8"));
        
        StringBuilder response = new StringBuilder();
        String inputLine = "";

        while ((inputLine = in.readLine()) != null) {
        	response.append(inputLine);
        }
        	
        in.close();     
        
		return response;
	}
	
	public static List<PullRequestDto> getGitHubPullRequests (GitHubConnection connection) {
		
		List<PullRequestDto> pullRequests = new ArrayList<>();
		JSONArray rawPullRequests = new JSONArray();
		JSONObject rawUser = new JSONObject();
		String pullsUrl = buildPullsUrl(connection.getUrl(), connection.getOwner(), connection.getProject(), connection.getBranchName());
		String token = buildToken(connection.getToken());
		
		try {
				
			rawPullRequests = new JSONArray(getRawData(pullsUrl, token).toString());
			
		} catch (Exception e) {e.printStackTrace();}
				
		if (!rawPullRequests.isEmpty()) {
			System.out.println("ovde sam");
			
			for (Object jo : rawPullRequests) {
				
				JSONObject obj = (JSONObject) jo;				
				String initials = "";
				
				try {
							
					rawUser = new JSONObject(getRawData(buildInitialsUrl(connection.getUrl(), obj.getJSONObject("user").optString("login")), token).toString());     		        
			        String fullName = rawUser.optString("name");	        
			        StringBuilder sb = new StringBuilder();
			        
			        if (!fullName.isEmpty()) {
			        	
			        	for (String s : fullName.split(" ")) {  		
			        		if (Character.isLetter(s.charAt(0))) {
			        			sb.append(s.charAt(0));
			        		}			
			            }			       	
			        	initials = sb.toString();		
			        	
			        }else { 			        	
			        	initials = "";   			        	
			        }   
					
				}catch(JSONException e){e.printStackTrace();}	catch(Exception e){e.printStackTrace();}
				
				String title= obj.optString("title");
				
				pullRequests.add(new PullRequestDto(initials,title));
				
			}						
		}
		
		return pullRequests;
	}

}
