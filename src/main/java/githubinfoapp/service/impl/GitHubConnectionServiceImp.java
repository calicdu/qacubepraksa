package githubinfoapp.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import githubinfoapp.model.GitHubConnection;
import githubinfoapp.repository.GitHubConnectionRepository;
import githubinfoapp.service.GitHubConnectionService;

@Service
@Transactional
public class GitHubConnectionServiceImp implements GitHubConnectionService {
	
	@Autowired
	GitHubConnectionRepository gitHubConnectionRepository;

	@Override
	public Page<GitHubConnection> findAll(int pageNum) {
		return gitHubConnectionRepository.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public GitHubConnection findOne(Long id) {
		return gitHubConnectionRepository.findOne(id);
	}

	@Override
	public void save(GitHubConnection connection) {
		gitHubConnectionRepository.save(connection);
		
	}

	@Override
	public GitHubConnection remove(Long id) {
		
		GitHubConnection connection = gitHubConnectionRepository.findOne(id);
		if(connection != null) {
			gitHubConnectionRepository.delete(connection);
		}
		
		return connection;
	}

	@Override
	public Page<GitHubConnection> search (String name, String project, String owner, String branchName, int page) {
		
		if(name != null ){
			name = "%" + name + "%";
		}
		if(project != null ){
			project = "%" + project + "%";
		}
		if(owner != null ){
			owner = "%" + owner + "%";
		}
		if(branchName != null ){
			branchName = "%" + branchName + "%";
		}
		
		return gitHubConnectionRepository.search(name, project, owner, branchName, new PageRequest(page, 5));
	}

}
