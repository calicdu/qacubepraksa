package githubinfoapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import githubinfoapp.model.GitHubConnection;


@Repository
public interface GitHubConnectionRepository extends JpaRepository<GitHubConnection, Long> {
	
	@Query("SELECT conn FROM GitHubConnection conn WHERE "
			+ "(:name IS NULL or conn.name like :name ) AND "
			+ "(:project IS NULL or conn.project like :project ) AND "
			+ "(:owner IS NULL OR conn.owner like :owner) AND "
			+ "(:branchName IS NULL OR conn.branchName like :branchName)"
			)
	
	Page<GitHubConnection> search (
			@Param("name") String name, 
			@Param("project") String project, 
			@Param("owner") String owner,
			@Param("branchName") String branchName,
			Pageable pageRequest);

}
