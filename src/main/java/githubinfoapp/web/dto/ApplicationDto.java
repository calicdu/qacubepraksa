package githubinfoapp.web.dto;

import java.util.ArrayList;
import java.util.List;


public class ApplicationDto {
	
	private Long id;	
	private String name;
	private List<ApplicationConnectionDto> connections  = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ApplicationConnectionDto> getConnections() {
		return connections;
	}
	public void setConnections(List<ApplicationConnectionDto> connections) {
		this.connections = connections;
	}

}
