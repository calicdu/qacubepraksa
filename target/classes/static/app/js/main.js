var githubInfoApp = angular.module("githubInfoApp", ['ngRoute']);

githubInfoApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/',{
        templateUrl: '/app/html/applications.html'
    }).when('/connections',{
        templateUrl: '/app/html/connections.html'
    }).otherwise({
        redirectTo: '/'
    });
}]);

//Applications

githubInfoApp.controller("applicationsCtrl", function($scope, $http){

	var baseUrlApplications = "/api/applications";
    var baseUrlConnections = "/api/githubconnections";

    $scope.operation = "Add";
    $scope.pageNum = 0;
    $scope.totalPages = 0;
    $scope.applications = [];
    $scope.application = {}; 
    $scope.searchApplication = {};
    $scope.connections = [];
    $scope.selectedConnections = [];
    $scope.pullRequests = [];
    $scope.defaultConnection = {};
    $scope.sortedConnections = [];

    // Get applications (with criteria)
    var getApplications = function(){

        var config = {params: {}};
        config.params.pageNum = $scope.pageNum;

        if($scope.searchApplication.name != ""){
            config.params.name = $scope.searchApplication.name;
        }

        $http.get(baseUrlApplications, config)
            .then(
            	function success(res){
            		$scope.applications = res.data;
            		$scope.totalPages = res.headers('totalPages');
            	},
            	function error(res){
            		alert("Cannot fetch applications!");
            	}
            );
    };


    // Get all available connections
    var getConnections = function(){

        $http.get(baseUrlConnections)
            .then(
            	function success(res){
                    $scope.connections = res.data;
                    $scope.connectionsBackup = res.data;
            	},
            	function error(res){
            		alert("Cannot fetch connections!");
            	}
            );

    };


    // Pack connections - take the ids from the select field, find connections and add them to the app
    var packConnections = function () {
        $scope.application.connections = [];
        $scope.selectedConnections.forEach(sC => {
            var selectedConnection = {};
            selectedConnection.isDefault = "";
            selectedConnection.connectionId = sC.connectionId;
            selectedConnection.connectionName = sC.connectionName;
            
            if(sC.connectionId == $scope.defaultConnection.connectionId) {
                selectedConnection.isDefault = true;
            }else {
                selectedConnection.isDefault = false;
            }

            $scope.application.connections.push(selectedConnection);
            $scope.selectedConnections = [];
        });
        
    }

    // Save - new connections - POST, edited connections PUT
    $scope.save = function(){
        packConnections();
        $scope.defaultConnection = {};
        if($scope.application.id == null) {
            $http.post(baseUrlApplications, $scope.application)
            .then(
            	function success(res){
            		getApplications();
                    $scope.application = {};
            	},
            	function error(res){
            		alert("Cannot save the app!");
            	}
            );
        }else {
            $http.put(baseUrlApplications + "/" + $scope.application.id, $scope.application)
            .then(
        		function success(res){
                    getApplications();
                    getConnections();
                    $scope.application = {};
        		},
        		function error(res){
        		}
            );
        }
        
    };

    // Delete
    $scope.delete = function(id){
        $http.delete(baseUrlApplications + "/" + id).then(
            function success(data){
            	getApplications();
            },
            function error(data){
                alert("Cannot delete the app!");
            }
        );
    }

    //Search connections by criteria
    $scope.search = function () {
        $scope.pageNum = 0;
        getApplications();
    }

    var createConnectionList = function () {
        
        var oldIds = [];
        $scope.connections = [];
        if (Object.keys($scope.application).length !== 0 && $scope.application.constructor !== Object) {
            $scope.sortedConnections = $scope.connections;
        }else {
            $scope.application.connections.forEach(aC => {
                oldIds.push(aC.connectionId);
            });
            $scope.connections = $.extend(true, [], $scope.connectionsBackup);
            $scope.connections = $scope.connections.map(obj =>{
                obj.selected = oldIds.includes(obj.connectionId); 
                if (oldIds.includes(obj.connectionId)) {
                    obj.connectionName = obj.connectionName.concat(' --- X');
                }
                return obj;
            });
        }    
    }

    
    // Edit - table button
    $scope.edit = function(id){

        $scope.operation = "Edit";
        // $scope.application = {};

        $http.get(baseUrlApplications + "/" + id) 
            .then(
            	function success(res){
                    $scope.application = res.data;
                    createConnectionList();
            	},
            	function error(data){
            		alert("Cannot fetch application!");
            	}
            ); 
    }



    // Get pull requests
    $scope.getPullRequests = function(id){
    	$http.get(baseUrlApplications + "/" + id + "/pullrequests").then(
    		function success(res){
    			$scope.pullRequests = res.data;
    		},
    		function error(res){
    			alert("Could not gett the Pull Requests!")
    		}
    	)
    }

    //Cancel
    $scope.cancel = function(){
        $scope.operation = "Add";
        $scope.application = {};
        $scope.selectedConnections = [];
        $scope.defaultConnection = {}; 
    }

    //Table navigation
    $scope.back = function(){
        if($scope.pageNum > 0) {
            $scope.pageNum = $scope.pageNum - 1;
            getApplications();
        }
    };

    $scope.next = function(){
        if($scope.pageNum < $scope.totalPages - 1){
            $scope.pageNum = $scope.pageNum + 1;
            getApplications();
        }
    };

    // Start

    getConnections();
    getApplications();
    // createConnectionList();

});

//Applications end!

// Connections

githubInfoApp.controller("connectionsCtrl", function($scope, $http, $location){

    var baseUrlConnections = "/api/githubconnections";

    $scope.operation = "Add";
    $scope.pageNum = 0;
    $scope.totalPages = 0;
    $scope.connections = [];
    $scope.connection = {};    
    $scope.searchConnection = {};

    //////////////////////////////////////////

    // Get connections (with criteria)
    var getConnections = function(){

        var config = {params: {}};
        config.params.pageNum = $scope.pageNum;

        if($scope.searchConnection.connectionName != ""){
            config.params.connectionName = $scope.searchConnection.connectionName;
        }
        if($scope.searchConnection.project != ""){
            config.params.project = $scope.searchConnection.project;
        }
        if($scope.searchConnection.owner != ""){
            config.params.owner = $scope.searchConnection.owner;
        }
        if($scope.searchConnection.branchName != ""){
            config.params.branchName = $scope.searchConnection.branchName;
        }

        $http.get(baseUrlConnections, config)
            .then(
            	function success(res){
            		$scope.connections = res.data;
            		$scope.totalPages = res.headers('totalPages');
            	},
            	function error(res){
            		alert("Cannot fetch connections!");
            	}
            );
    };

    // Save - new connections - POST, edited connections PUT
    $scope.save = function(){

        if($scope.connection.connectionId == null) {
            $http.post(baseUrlConnections, $scope.connection)
            .then(
            	function success(res){
                    getConnections();
                    $scope.connection = {};
            	},
            	function error(res){
            	}
            );

        }else {
            $http.put(baseUrlConnections + "/" + $scope.connection.connectionId, $scope.connection)
            .then(
        		function success(res){
                    getConnections();
                    $scope.connection = {};
        		},
        		function error(res){
        		}
            );
        }      
    };

    // Edit - table button
    $scope.edit = function(id){

        $scope.operation = "Edit";
        $scope.connection = {};

        $http.get(baseUrlConnections + "/" + id) 
            .then(
            	function success(res){
            		$scope.connection = res.data;
            	},
            	function error(data){
            		alert("Cannot fetch connection!");
            	}
            ); 
    }

    // Delete
    $scope.delete = function(id){
        $http.delete(baseUrlConnections + "/" + id).then(
            function success(data){
            	getConnections();
            },
            function error(data){
                alert("Cannot delete connection!");
            }
        );
    }

    //Search connections by criteria
    $scope.search = function () {
        $scope.pageNum = 0;
        getConnections();
    }

    //Cancel
    $scope.cancel = function(){
        $scope.operation = "Add";
        $scope.connection = {};     
    }

    //Table navigation
    $scope.back = function(){
        if($scope.pageNum > 0) {
            $scope.pageNum = $scope.pageNum - 1;
            getConnections();
        }
    };

    $scope.next = function(){
        if($scope.pageNum < $scope.totalPages - 1){
            $scope.pageNum = $scope.pageNum + 1;
            getConnections();
        }
    };

    // Start

    getConnections();

});

// Connections end!


