package githubinfoapp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import githubinfoapp.model.ApplicationConnection;
import githubinfoapp.service.GitHubConnectionService;
import githubinfoapp.web.dto.ApplicationConnectionDto;

@Component
public class ApplicationConnectionToDto implements Converter<ApplicationConnection, ApplicationConnectionDto> {
	
	@Autowired
	GitHubConnectionService gitHubConnectionService;

	@Override
	public ApplicationConnectionDto convert(ApplicationConnection source) {
		
		ApplicationConnectionDto dto = new ApplicationConnectionDto();
		dto.setConnectionName(source.getConnection().getName());
		dto.setIsDefault(source.getIsDefault());
		dto.setConnectionId(source.getConnection().getId());
		return dto;
	}
	
	public List<ApplicationConnectionDto> convert(List<ApplicationConnection> connections){
		List<ApplicationConnectionDto> dto = new ArrayList<>();
		
		for(ApplicationConnection a : connections){
			dto.add(convert(a));
		}
		
		return dto;
	}

}
