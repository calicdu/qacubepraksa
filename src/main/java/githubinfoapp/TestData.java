package githubinfoapp;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import githubinfoapp.model.Application;
import githubinfoapp.model.GitHubConnection;
import githubinfoapp.service.ApplicationService;
import githubinfoapp.service.GitHubConnectionService;


@Component
public class TestData {
	
	@Autowired
	private GitHubConnectionService gitHubConnectionService;
	
	@Autowired
	private ApplicationService applicationService;
	
	@PostConstruct
	public void init() {
		
		GitHubConnection conn1 = new GitHubConnection();
		conn1.setBranchName("master");
		conn1.setName("VSCode Icons");
		conn1.setOwner("vscode-icons");
		conn1.setProject("vscode-icons");
		conn1.setToken("9bd9a7e1c2e32a005743e260701fccd7acd14279");
		conn1.setUrl("https://github.com");
		gitHubConnectionService.save(conn1);
		
		GitHubConnection conn2 = new GitHubConnection();
		conn2.setBranchName("master");
		conn2.setName("Microsoft VSCODE");
		conn2.setOwner("Microsoft");
		conn2.setProject("vscode");
		conn2.setToken("9bd9a7e1c2e32a005743e260701fccd7acd14279");
		conn2.setUrl("https://github.com");
		gitHubConnectionService.save(conn2);
		
		GitHubConnection conn3 = new GitHubConnection();
		conn3.setBranchName("dcalic-patch-1");
		conn3.setName("QACube Github");
		conn3.setOwner("dcalic");
		conn3.setProject("zadatak04data");
		conn3.setToken("9bd9a7e1c2e32a005743e260701fccd7acd14279");
		conn3.setUrl("https://github.qacube.com");
		gitHubConnectionService.save(conn3);
		
		Application app1 = new Application();
		app1.setName("Visual Studio Code");
		app1.addConnection(conn1, true);
		app1.addConnection(conn2, false);
		app1.addConnection(conn3, false);
		applicationService.save(app1);
		
		Application app2 = new Application();
		app2.setName("QACube Github");
		app2.addConnection(conn3, true);
		app2.addConnection(conn1, false);	
		applicationService.save(app2);
		
		Application app3 = new Application();
		app3.setName("Empty");
		applicationService.save(app3);
	}
}