package githubinfoapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import githubinfoapp.model.GitHubConnection;


public interface GitHubConnectionService {
	
	Page<GitHubConnection> findAll(int pageNum);

	GitHubConnection findOne(Long id);
	void save(GitHubConnection connection);
	GitHubConnection remove(Long id);
	
	Page<GitHubConnection> search (
			@Param("name") String name, 
			@Param("project") String project, 
			@Param("owner") String owner,
			@Param("branchName") String branchName,
			int page);
	
}
