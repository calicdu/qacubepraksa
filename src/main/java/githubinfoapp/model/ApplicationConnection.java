package githubinfoapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table
public class ApplicationConnection {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
    @JoinColumn(name = "application_id")
	private Application application;
	
	@ManyToOne
    @JoinColumn(name = "connection_id")
	private GitHubConnection connection;
	
	@Column(name = "is_default")
	private Boolean isDefault;
	
	public ApplicationConnection(Application application, GitHubConnection connection, Boolean isDefault) {
		super();
		this.application = application;
		this.connection = connection;
		this.isDefault = isDefault;
	}

	public ApplicationConnection() {
		super();
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public GitHubConnection getConnection() {
		return connection;
	}

	public void setConnection(GitHubConnection connection) {
		this.connection = connection;
	}
	
}
