package githubinfoapp.web.dto;

public class PullRequestDto {
	
	private String initials;
	private String title;
	
	
	public String getInitials() {
		return initials;
	}
	public void setInitials(String initials) {
		this.initials = initials;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public PullRequestDto(String initials, String title) {
		super();
		this.initials = initials;
		this.title = title;
	}
	
	
	public PullRequestDto() {
		super();
	}
	
	
	
	

}
