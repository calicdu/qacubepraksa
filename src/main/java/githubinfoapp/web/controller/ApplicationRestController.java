package githubinfoapp.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import githubinfoapp.model.Application;
import githubinfoapp.service.ApplicationService;
import githubinfoapp.support.ApplicationToDto;
import githubinfoapp.support.DtoToApplication;
import githubinfoapp.web.dto.ApplicationDto;
import githubinfoapp.web.dto.PullRequestDto;


@RestController
@RequestMapping("/api/applications")

public class ApplicationRestController {

	@Autowired
	private ApplicationService applicationService;
	
	@Autowired
	private ApplicationToDto toApplicationDto;
	
	@Autowired
	private DtoToApplication toApplication;
	
	@Autowired
	   RestTemplate restTemplate;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ApplicationDto>> get(
			
			@RequestParam(required=false) String name,
			@RequestParam(defaultValue="0") int pageNum){
		
		Page<Application> applications;
		
		if(name != null) {
			applications = applicationService.search(name, pageNum);
		}else{
			applications = applicationService.findAll(pageNum);
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(applications.getTotalPages()));
				
		return new ResponseEntity<>(toApplicationDto.convert(applications.getContent()), headers, HttpStatus.OK);
		
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public ResponseEntity<ApplicationDto> get(@PathVariable Long id){
		
		Application application = applicationService.findOne(id);	
		if(application == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}	
		return new ResponseEntity<>(toApplicationDto.convert(application), HttpStatus.OK);
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<ApplicationDto> add(@RequestBody ApplicationDto newApplication){
		
		Application application = toApplication.convert(newApplication); 
		application = applicationService.addConnections(applicationService.save(application), newApplication.getConnections());
		
		return new ResponseEntity<>(toApplicationDto.convert(application), HttpStatus.CREATED);
		
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/{id}")
	public ResponseEntity<ApplicationDto> edit(@PathVariable Long id, @RequestBody ApplicationDto editedApplication){
		
		if(!id.equals(editedApplication.getId())){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}		
		Application application = toApplication.convert(editedApplication); 
		application = applicationService.addConnections(applicationService.save(application), editedApplication.getConnections());

		return new ResponseEntity<>(toApplicationDto.convert(application), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	public ResponseEntity<ApplicationDto> delete(@PathVariable Long id){
		
		Application deleted = applicationService.remove(id);	
		if(deleted == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}	
		return new ResponseEntity<>(toApplicationDto.convert(deleted), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}/pullrequests")
	public ResponseEntity<List<PullRequestDto>> getPullRequests (@PathVariable Long id){
		
		List<PullRequestDto> pullRequests = applicationService.getPullRequests(id);
		
		if (pullRequests == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
				
		return new ResponseEntity<>(pullRequests, HttpStatus.OK);

	}
	
	
	@ExceptionHandler
	public ResponseEntity<Void> validationHandler(DataIntegrityViolationException e){
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
}
