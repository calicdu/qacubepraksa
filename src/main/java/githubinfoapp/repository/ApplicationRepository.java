package githubinfoapp.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import githubinfoapp.model.Application;


@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {
	
	@Query("SELECT a FROM Application a WHERE (:name IS NULL or a.name like :name )")

	Page<Application> search (@Param("name") String name, Pageable pageRequest);
	
	
}
