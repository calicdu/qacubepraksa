package githubinfoapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import githubinfoapp.model.ApplicationConnection;

@Repository
public interface ApplicationConnectionRepository extends JpaRepository<ApplicationConnection, Long> {
	
	void deleteByApplicationId (long applicationId);

}
