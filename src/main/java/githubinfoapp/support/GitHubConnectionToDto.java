package githubinfoapp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import githubinfoapp.model.GitHubConnection;
import githubinfoapp.web.dto.GitHubConnectionDto;

@Component
public class GitHubConnectionToDto implements Converter<GitHubConnection, GitHubConnectionDto> {

	@Override
	public GitHubConnectionDto convert(GitHubConnection source) {
		
		GitHubConnectionDto dto = new GitHubConnectionDto();
		
		dto.setBranchName(source.getBranchName());
		dto.setConnectionId(source.getId());
		dto.setConnectionName(source.getName());
		dto.setOwner(source.getOwner());
		dto.setProject(source.getProject());
		dto.setToken(source.getToken());
		dto.setUrl(source.getUrl());
		
		return dto;
	}
	
	public List<GitHubConnectionDto> convert(List<GitHubConnection> connections){
		List<GitHubConnectionDto> dto = new ArrayList<>();
		
		for(GitHubConnection c : connections){
			dto.add(convert(c));
		}
		
		return dto;
	}

}
