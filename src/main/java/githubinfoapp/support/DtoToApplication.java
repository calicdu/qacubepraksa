package githubinfoapp.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import githubinfoapp.model.Application;
import githubinfoapp.service.ApplicationService;
import githubinfoapp.web.dto.ApplicationDto;

@Component
public class DtoToApplication implements Converter<ApplicationDto, Application> {
	
	@Autowired
	private ApplicationService applicationService;

	@Override
	public Application convert(ApplicationDto source) {

		Application application;
		
		if(source.getId()==null){
			
			application = new Application();
			
		}else{
			
			application = applicationService.findOne(source.getId());
			
		}
		
		application.setName(source.getName());
		
		return application;
	}

}
