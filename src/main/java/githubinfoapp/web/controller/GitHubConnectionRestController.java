package githubinfoapp.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import githubinfoapp.model.GitHubConnection;
import githubinfoapp.service.GitHubConnectionService;
import githubinfoapp.support.DtoToGitHubConnection;
import githubinfoapp.support.GitHubConnectionToDto;
import githubinfoapp.web.dto.GitHubConnectionDto;


@RestController
@RequestMapping("/api/githubconnections")

public class GitHubConnectionRestController {

	@Autowired
	private GitHubConnectionService gitHubConnectionService;
	
	@Autowired
	private GitHubConnectionToDto toGitHubConnectionDto;
	
	@Autowired
	private DtoToGitHubConnection toGitHubConnection;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<GitHubConnectionDto>> get(
			
			@RequestParam(required=false) String connectionName,
			@RequestParam(required=false) String project,
			@RequestParam(required=false) String owner,
			@RequestParam(required=false) String branchName,
			@RequestParam(defaultValue="0") int pageNum){
		
		Page<GitHubConnection> connections;
		
		if(connectionName != null || project != null || owner != null || branchName != null) {
			connections = gitHubConnectionService.search(connectionName, project, owner, branchName, pageNum);
		}else{
			connections = gitHubConnectionService.findAll(pageNum);
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(connections.getTotalPages()));
					
		return new ResponseEntity<>(toGitHubConnectionDto.convert(connections.getContent()), headers, HttpStatus.OK);
		
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public ResponseEntity<GitHubConnectionDto> get(@PathVariable Long id){
		
		GitHubConnection connection = gitHubConnectionService.findOne(id);	
		if(connection == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}	
		return new ResponseEntity<>(toGitHubConnectionDto.convert(connection), HttpStatus.OK);
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<GitHubConnectionDto> add(@RequestBody GitHubConnectionDto newConnection){
		
		GitHubConnection connection = toGitHubConnection.convert(newConnection); 
		gitHubConnectionService.save(connection);
		return new ResponseEntity<>(toGitHubConnectionDto.convert(connection), HttpStatus.CREATED);
		
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/{id}")
	public ResponseEntity<GitHubConnectionDto> edit(@PathVariable Long id, @RequestBody GitHubConnectionDto editedConnection){
		
		if(!id.equals(editedConnection.getConnectionId())){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}		
		GitHubConnection connection = toGitHubConnection.convert(editedConnection); 
		gitHubConnectionService.save(connection);	
		return new ResponseEntity<>(toGitHubConnectionDto.convert(connection), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	public ResponseEntity<GitHubConnectionDto> delete(@PathVariable Long id){
		
		GitHubConnection deleted = gitHubConnectionService.remove(id);	
		if(deleted == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}	
		return new ResponseEntity<>(toGitHubConnectionDto.convert(deleted), HttpStatus.OK);
	}
	
	
	@ExceptionHandler
	public ResponseEntity<Void> validationHandler(DataIntegrityViolationException e){
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
}
