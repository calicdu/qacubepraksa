package githubinfoapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import githubinfoapp.model.Application;
import githubinfoapp.model.ApplicationConnection;
import githubinfoapp.model.GitHubConnection;
import githubinfoapp.repository.ApplicationConnectionRepository;
import githubinfoapp.repository.ApplicationRepository;
import githubinfoapp.repository.GitHubConnectionRepository;
import githubinfoapp.service.ApplicationService;
import githubinfoapp.support.GitHubUtil;
import githubinfoapp.web.dto.ApplicationConnectionDto;
import githubinfoapp.web.dto.PullRequestDto;

@Service
@Transactional
public class ApplicationServiceImpl implements ApplicationService {
	
	@Autowired
	private ApplicationRepository applicationRepository;
	@Autowired
	private GitHubConnectionRepository gitHubConnectionRepository;
	@Autowired
	private ApplicationConnectionRepository applicationConnectionRepository;
	
	@Override
	public Page<Application> findAll(int pageNum) {
		return applicationRepository.findAll(new PageRequest(pageNum, 5));
	}
	
	@Override
	public Page<Application> search(String name, int page) {
		
		if(name != null ){
			name = "%" + name + "%";
		}
		
		return applicationRepository.search(name, new PageRequest(page, 5));
	}

	@Override
	public Application findOne(Long id) {
		return applicationRepository.findOne(id);
	}

	@Override
	public Application save(Application application) {
		
		return applicationRepository.save(application);
		
	}
	
	@Override
	public Application remove(Long id) {
		
		Application application = applicationRepository.findOne(id);
		if(application != null) {
			applicationRepository.delete(application);
		}
		
		return application;
	}
	
	@Override
	public Application addConnections(Application application, List<ApplicationConnectionDto> connections) {
		
		GitHubConnection connection;
		applicationConnectionRepository.deleteByApplicationId(application.getId());
		
		for(ApplicationConnectionDto appConDto : connections){
			
			if (appConDto != null && gitHubConnectionRepository.findOne(appConDto.getConnectionId()) != null) {
				connection = gitHubConnectionRepository.findOne(appConDto.getConnectionId());
				application.addConnection(connection, appConDto.getIsDefault());
			}
			
		}
		return application;
	}

	@Override
	public List<PullRequestDto> getPullRequests(Long id) {
		
		List<PullRequestDto> pullRequests = new ArrayList<>();
		Application application = applicationRepository.findOne(id);
		
		
		if (application.getConnections().isEmpty()) {
			return pullRequests;
		}
		
		if (application != null) {
			ApplicationConnection apppCon = application.getConnections().stream().filter(e -> e != null).filter(e -> e.getIsDefault() == true)
					.findFirst().orElse(application.getConnections().get(0));

GitHubConnection connection = apppCon.getConnection();		
pullRequests = GitHubUtil.getGitHubPullRequests(connection);
		}
				
		ApplicationConnection apppCon = application.getConnections().stream().filter(e -> e != null).filter(e -> e.getIsDefault() == true)
											.findFirst().orElse(application.getConnections().get(0));
		
		GitHubConnection connection = apppCon.getConnection();		
		pullRequests = GitHubUtil.getGitHubPullRequests(connection);
		
		return pullRequests;
		
	}
	
	
	
}
