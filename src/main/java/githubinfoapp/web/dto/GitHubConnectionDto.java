package githubinfoapp.web.dto;

public class GitHubConnectionDto {
	
	private Long connectionId;
	private String connectionName;
	private String url;
	private String token;
	private String project;
	private String owner;
	private String branchName;
	
	
	public Long getConnectionId() {
		return connectionId;
	}
	public void setConnectionId(Long id) {
		this.connectionId = id;
	}
	public String getConnectionName() {
		return connectionName;
	}
	public void setConnectionName(String name) {
		this.connectionName = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


}
