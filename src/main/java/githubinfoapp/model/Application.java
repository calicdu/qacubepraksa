package githubinfoapp.model;

import java.util.ArrayList;
//import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Application {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String name;
	
	@OneToMany(mappedBy="application", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ApplicationConnection> connections  = new ArrayList<>();

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ApplicationConnection> getConnections() {
		return connections;
	}

	public void setConnections(List<ApplicationConnection> connections) {
		this.connections = connections;
	}
	
	public void addConnection(GitHubConnection connection, boolean isDefault) {
		ApplicationConnection association = new ApplicationConnection();
		association.setConnection(connection);
		association.setApplication(this);
		association.setIsDefault(isDefault);
		this.connections.add(association);
		connection.getApplications().add(association);
	}
	
//	 public void removeConnection(GitHubConnection connection) {
//		 for (Iterator<ApplicationConnection> iterator = connections.iterator(); iterator.hasNext(); ) {
//			 ApplicationConnection association = iterator.next();
//			 if (association.getApplication().equals(this) && association.getConnection().equals(connection)) {
//				 iterator.remove();
//				 association.getConnection().getApplications().remove(association);
//				 association.setApplication(null);
//				 association.setConnection(null);
//	         }
//	     }
//	 }
 
}
