package githubinfoapp.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import githubinfoapp.model.GitHubConnection;
import githubinfoapp.service.GitHubConnectionService;
import githubinfoapp.web.dto.GitHubConnectionDto;

@Component
public class DtoToGitHubConnection implements Converter<GitHubConnectionDto, GitHubConnection> {
	
	@Autowired
	private GitHubConnectionService gitHubConnectionService;
	
	@Override
	public GitHubConnection convert(GitHubConnectionDto source) {
		
		GitHubConnection connection;
		
		if(source.getConnectionId()==null){
			
			connection = new GitHubConnection();
			
		}else{
			
			connection = gitHubConnectionService.findOne(source.getConnectionId());
			
		}
		
		connection.setBranchName(source.getBranchName());
		connection.setName(source.getConnectionName());
		connection.setOwner(source.getOwner());
		connection.setProject(source.getProject());
		connection.setToken(source.getToken());
		connection.setUrl(source.getUrl());
		
		return connection;
	}

}
