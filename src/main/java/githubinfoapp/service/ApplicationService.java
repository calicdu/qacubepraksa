package githubinfoapp.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import githubinfoapp.model.Application;
import githubinfoapp.web.dto.ApplicationConnectionDto;
import githubinfoapp.web.dto.PullRequestDto;

public interface ApplicationService {
	
	Page<Application> findAll(int pageNum);
	Page<Application> search(@Param("name") String name, int page);
	Application findOne(Long id);
	Application save(Application application);
	Application remove(Long id);
	Application addConnections(Application application, List<ApplicationConnectionDto> connections);
	List<PullRequestDto> getPullRequests (Long id);

}
