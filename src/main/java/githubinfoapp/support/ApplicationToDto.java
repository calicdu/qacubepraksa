package githubinfoapp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import githubinfoapp.model.Application;
import githubinfoapp.web.dto.ApplicationDto;

@Component
public class ApplicationToDto implements Converter<Application, ApplicationDto> {
	
	@Autowired
	ApplicationConnectionToDto appConToDto;

	@Override
	public ApplicationDto convert(Application source) {
		
		ApplicationDto dto = new ApplicationDto();
		dto.setId(source.getId());
		dto.setName(source.getName());
		dto.setConnections(appConToDto.convert(source.getConnections()));
		return dto;
	}
	
	public List<ApplicationDto> convert(List<Application> applications){
		List<ApplicationDto> dto = new ArrayList<>();
		
		for(Application a : applications){
			dto.add(convert(a));
		}
		
		return dto;
	}

}
